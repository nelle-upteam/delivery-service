<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
   

    

    Route::resource('/routes', 'RoutesController')->only([
        'index', 'store', 'destroy', 'show', 'update'
    ]);
    
    Route::resource('/route-connections', 'RouteConnectionsController')->only([
        'index', 'store', 'destroy', 'show', 'update']);
    
    Route::post('/refresh-token', 'ApiTokenController@update');

    Route::post('/logout', 'LoginController@logout');

});

Route::post('/route-search', 'RouteSearchController@routeSearch');

Route::post('/login', 'LoginController@login');

Route::post('/register','RegisterController@create');
