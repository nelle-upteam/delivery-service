<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routes extends Model
{
    public function connections()
    {
        return $this->hasMany('App\RouteConnections');
    }
}
