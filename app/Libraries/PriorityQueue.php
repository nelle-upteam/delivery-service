<?php

namespace App\Libraries;

class PriorityQueue {

    public $values;

    function __construct()
    {
        $this->values = [];
    }
    
    function enqueue($val, $priority) 
    {
        array_push($this->values,[$val, $priority]);
        $this->sort();
    }

    function dequeue() 
    {
        return array_shift($this->values);
    }

    function sort() 
    {
        uasort($this->values, function($a, $b) {
            return $a[1] - $b[1];
        });
    }

}