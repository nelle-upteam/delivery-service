<?php 

namespace App\Libraries;

use App\Libraries\PriorityQueue;
use stdClass;

class WeightedGraph {

    public $adjacencyList;
    
    function __construct()
    {
        $this->adjacencyList = array();    
    }


    function addVertex($name) 
    {
        if(!isset($this->adjacencyList[$name])) 
        {
            $this->adjacencyList[$name] = array();
        }
    }

    function addEdge($vert1, $vert2, $weight) 
    {
        $this->adjacencyList[$vert1][$vert2] = $weight;
        $this->adjacencyList[$vert2][$vert1] = $weight;
    }

    function removeEdge($v1, $v2) 
    {
        unset($this->adjacencyList[$v1][$v2]);
        unset($this->adjacencyList[$v2][$v1]);
    }

    function removeVertex($vert) 
    {
        foreach($this->adjacencyList[$vert] as $key => $v) 
        {
            $this->removeEdge($vert, $key);
        }

         unset($this->adjacencyList[$vert]);
    }

    function dijkstras($start, $finish, $prioWght) 
    {
        // List1
        $costFromStartTo = [];
        // List2
        $checkList = new PriorityQueue();
        // List3
        $prev = [];

        $current = '';
        $result = [];

        foreach ($this->adjacencyList as $vert => $vertVal) 
        {
            if ($vert === $start) 
            {
                $costFromStartTo[$vert] = 0;
                $checkList->enqueue($vert, 0);
            } 
            else 
            {
                $costFromStartTo[$vert] = INF;
            }

            $prev[$vert] = null;
        }

        // remove end point if a neighbor
        if(isset($this->adjacencyList[$start][$finish])) 
        {
            unset($this->adjacencyList[$start][$finish]);
        } 

        while (count($checkList->values)) 
        {
            $current = $checkList->dequeue()[0];
            
            if ($current === $finish) 
            {
                // Done
                while ($prev[$current]) 
                {
                    array_push($result,$current);
                    $current = $prev[$current];
                }
                break;
            }
            else 
            {
                foreach ($this->adjacencyList[$current] as $neighbor => $weight) 
                {   
                    $costToNeighbor = $costFromStartTo[$current] + $this->adjacencyList[$current][$neighbor][$prioWght];

                    if ($costToNeighbor < $costFromStartTo[$neighbor]) 
                    {
                        $costFromStartTo[$neighbor] = $costToNeighbor;
                        $prev[$neighbor] = $current;
                        $checkList->enqueue($neighbor, $costToNeighbor);
                    }
                }
                
            }
        }

        array_push($result,$current);

        return array_reverse($result);
        
    }






}