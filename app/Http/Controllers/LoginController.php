<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    function login(Request $request) 
    {
        
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) 
        {
            Auth::user()->generateToken();
            
            return response()->json([
                'message' => 'success',
                'data' => Auth::user(),
            ]);

        }
    
        return $this->sendFailedLoginResponse($request);
       
    }

    function logout() 
    {
        $user = Auth::guard('api')->user();
        
        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return response()->json([
            'message' => 'success',
            'data' => 'User logged out.'
        ]);
        
    }
}
