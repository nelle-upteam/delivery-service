<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\WeightedGraph;
use App\Routes;

class RouteSearchController extends Controller
{
    function index() 
    {
        
        $routeArr = array();

        $routes = Routes::get();

        $route = new WeightedGraph();

        foreach ($routes as $key => $vertex) 
        {
            $routeArr[$vertex->id] = $vertex->name;
            $route->addVertex($vertex->name);
        }

        // $route->addVertex("A-route");
        // $route->addVertex("B-route");
        // $route->addVertex("C-route");
        // $route->addVertex("D-route");
        // $route->addVertex("E-route");
        // $route->addVertex("F-route");
        // $route->addVertex("G-route");
        // $route->addVertex("H-route");
        // $route->addVertex("I-route");

        foreach ($routes as $key => $vertexes) {
            $neighbor = $vertexes->connections()->get()->toArray();
            
            foreach ($neighbor as $vertex) {
                $route->addEdge($routeArr[$vertex['routes_id']], $routeArr[$vertex['routes_id_neighbor']], ['cost'=> $vertex['cost'], 'time' => $vertex['time'] ]);
            }
        }
        
        // $route->addEdge("A-route", "C-route", ['cost'=>20, 'time' => 1]);
        // $route->addEdge("A-route", "E-route", ['cost'=>5, 'time' => 30]);
        // $route->addEdge("A-route", "H-route", ['cost'=>1, 'time' => 10]);

        // $route->addEdge("C-route", "B-route", ['cost'=>12, 'time' => 1]);

        // $route->addEdge("H-route", "E-route", ['cost'=>1, 'time' => 30]);

        // $route->addEdge("D-route", "E-route", ['cost'=>5, 'time' => 3]);
        // $route->addEdge("D-route", "F-route", ['cost'=>50, 'time' => 4]);
        
        // $route->addEdge("F-route", "I-route", ['cost'=>50, 'time' => 45]);
        // $route->addEdge("F-route", "G-route", ['cost'=>40, 'time' => 50]);

        // $route->addEdge("G-route", "B-route", ['cost'=>73, 'time' => 64]);

        $result = $route->dijkstras('A-route','B-route', 'cost');

        return response($result);
    }

    function routeSearch(Request $request) 
    {
        $routeArr = array();

        $routes = Routes::get();

        $route = new WeightedGraph();

        foreach ($routes as $key => $vertex) 
        {
            $routeArr[$vertex->id] = $vertex->name;
            $route->addVertex($vertex->name);
        }

        foreach ($routes as $key => $vertexes) 
        {
            $neighbor = $vertexes->connections()->get()->toArray();
            
            foreach ($neighbor as $vertex) 
            {
                $route->addEdge($routeArr[$vertex['routes_id']], $routeArr[$vertex['routes_id_neighbor']], ['cost'=> $vertex['cost'], 'time' => $vertex['time'] ]);
            }
        }
        
        $result = $route->dijkstras($request->start,$request->end, $request->priority);

        return response($result);
        
    }
}
