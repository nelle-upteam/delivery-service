<?php

namespace App\Http\Controllers;

use App\RouteConnections;
use Illuminate\Http\Request;

class RouteConnectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rC = RouteConnections::get()->toArray();

        return response($rC);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rC = new RouteConnections();

        $rC->routes_id = $request->routes_id;
        $rC->routes_id_neighbor = $request->routes_id_neighbor;
        $rC->cost = $request->cost;
        $rC->time = $request->time;

        if($rC->save()) 
        {
            return response($rC);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rC = RouteConnections::findOrFail($id);

        return response($rC);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rC = RouteConnections::findOrFail($id);

        $rC->routes_id = $request->routes_id;
        $rC->routes_id_neighbor = $request->routes_id_neighbor;
        $rC->cost = $request->cost;
        $rC->time = $request->time;
        
        $rC->save();
        
        return response($rC);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rC = RouteConnections::findOrFail($id);
        if($rC->delete())
        {
            return 'deleted';
        }
        
    }
}
