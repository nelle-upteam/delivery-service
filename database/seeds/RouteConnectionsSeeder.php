<?php

use Illuminate\Database\Seeder;

class RouteConnectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routesCon = array(
            array(1, 3, 20, 1),
            array(1, 5, 5, 30),
            array(1, 8, 1, 10),
            array(3, 2, 12, 1),
            array(8, 5, 1, 30),
            array(4, 5, 5, 3),
            array(4, 6, 50, 4),
            array(6, 9, 50, 45),
            array(6, 7, 40, 50),
            array(7, 2, 73, 64)
        );

        foreach ($routesCon as $route) {
            DB::table('route_connections')->insert([
                'routes_id' => $route[0],
                'routes_id_neighbor' => $route[1],
                'cost' => $route[2],
                'time' => $route[3],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
